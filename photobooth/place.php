<?php
// Menggunakan Midtrans PHP Library
require_once dirname(__FILE__) . '/mid/Midtrans.php';

// Pastikan metode yang digunakan adalah POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Set server key dan konfigurasi lainnya
    \Midtrans\Config::$serverKey = 'SB-Mid-server-mbrLFKASErrxkEEZqmvs52Xx';
    \Midtrans\Config::$isProduction = false;
    \Midtrans\Config::$isSanitized = true;
    \Midtrans\Config::$is3ds = true;

    // Pastikan nilai total dan email sudah ada
    $json = file_get_contents('php://input');
    $data = json_decode($json, true);

    if (isset($data['total']) && isset($data['email'])) {
        // Mengambil nilai dari data JSON yang diterima
        $total = $data['total'];
        $email = $data['email'];
        $identitas = $data['identity'];
        $alamat = $data['alamat'];


        // Menyiapkan parameter untuk pembayaran ke Midtrans
        $params = array(
            "transaction_details" => array(
                "order_id" => rand(),
                "gross_amount" => $total
            ),
            "customer_details" => array(
                "first_name" => $identitas,
                "email" => $email,
                "phone" => "+628123456789",
                "billing_address" => array(
                    "address" => $alamat,
                ),
            ),
            "enabled_payments" => array(
                "gopay"
            ),
            "gopay" => array(
                "secure" => true
            )
        );

        // Meminta token dari Midtrans API
        $snapToken = \Midtrans\Snap::getSnapToken($params);

        // Mengembalikan token untuk digunakan dalam pembayaran
        echo $snapToken;
    } else {
        // Jika nilai total dan email tidak tersedia
        echo "Error: Total and email values are missing!";
    }
} else {
    // Jika form tidak disubmit menggunakan metode POST
    echo "Error: Form must be submitted using POST method!";
}

?>
